let baseURL;

switch (process.env.NODE_ENV) {
  case 'development': // 开发环境
    baseURL = 'http://dev-mall-pre.springboot.cn/api';
    break;
  case 'test': // 测试环境
    baseURL = 'http://test-mall-pre.springboot.cn/api';
    break;
  case 'prev':
    baseURL = 'http://prev-mall-pre.springboot.cn/api';
    break; 
  case 'prod': // 线上环境
    baseURL = 'http://mall-pre.springboot.cn/api';
    break;
  default: // 没有匹配到默认走线上环境
    baseURL = 'http://mall-pre.springboot.cn/api';
    break;
}

export default {
  baseURL
}