import Vue from 'vue';
import router from './router'; // 把写好的路由引入进来
import axios from 'axios'; // 每个用到axios的页面都要引入axios  
import VueAxios from 'vue-axios'; // 把作用域对象挂载到vue实例中去，方便我们用this去调用
import VueLazyload from 'vue-lazyload';
import VueCookie from 'vue-cookie';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from './store/index.js';
import App from './App.vue';
// import env from './env'

// mock开关
const mock = false;
if(mock){
  require('./mock/api');
}


// 根据前端的跨域方式做调整  /a/b ： /api/a/b => /a/b
axios.defaults.baseURL = '/api';
axios.defaults.timeout = 8000; // 设置8s的超时时间

// 根据环境变量获取不同的请求地址
// axios.defaults.baseURL = env.baseURL;
// 接口错误拦截
axios.interceptors.response.use(function(response){ // 响应(返回值)的拦截
  // console.log(response)
  let res = response.data; // 获取接口返回值
  let path = location.hash;
  if (res.status == 0) { // 成功
    return res.data;
  } else if (res.status == 10) { // 登陆拦截：没有登录时，接口报错，状态码为10
    if ( path != '#/index') { // 如果是登录页面就不用做登录的拦截。如果是首页就不需要登录跳转
      window.location.href = '/#/login'; // 跳转到登录页面
    }
    return Promise.reject(res);
  } else { // 失败
    alert(res.msg);
    return Promise.reject(res);
  }
})

Vue.use(VueAxios, axios) // 应用中间件 加载插件  把axios加载上去
Vue.use(VueCookie);
Vue.config.productionTip = false // 生产环境的提示，默认是false

Vue.use(VueLazyload,{
  // loading: require('/imgs/loading-svg/loading-bars.svg')
  loading: '/imgs/loading-svg/loading-bars.svg'
})

Vue.use(ElementUI);

new Vue({
  store, // 引入的store对象需要放到vue实例里去
  router, // 如果键值都是router的话，可以简写为：router
  render: h => h(App),
}).$mount('#app')
