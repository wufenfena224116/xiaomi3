/**
 * 商城Vuex-actions
*/

// 传输
export default {
  saveUserName(context, username){
    context.commit('saveUserName', username); // 通过commit会调用mutation
  },
  saveCartCount(context, count){
    context.commit('saveCartCount', count);
  }
}