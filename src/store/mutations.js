/**
 * 商城Vuex-mutations
*/

// 读取
export default {
  saveUserName(state, username){ // 最后一步是改变状态
    state.username = username;
  },
  saveCartCount(state, count){
    state.cartCount = count;
  }
}