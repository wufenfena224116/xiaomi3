module.exports = {
  devServer: { // 默认会自动加载devServer里的配置表
    host: 'localhost', // 访问的主机
    port: 8080, // 端口
    proxy: { // 代理：访问了/a接口，实际上代理了/b，那么真正访问的是/b接口
      '/api': {
        // target: 'https://www.imooc.com', // 当访问(拦截)到api时，代理到target地址里
        target: 'http://mall-pre.springboot.cn',
        changeOrigin: true, // 拦截到api时，把主机的低点设为原点
        pathRewrite: {
          '/api': '' // 路径的转发规则：当拦截到api时，把api置为空（实际上会把后面的地址当做真实的接口地址，而拼接到target目标源上面去，来进行转发）
        }
      }
    }
  }
}